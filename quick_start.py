#!/bin/env python
# <lewis.r.liu@gmail.com>
# Aug 12, 2015
"""
By <lewis.r.liu@gmail.com>
For sighline

Aug 12, 2015

Quickstart script to generate a random forest model using sklearn

Required libraries:
    sklearn
    pandas
"""

#+-----------+
#| load data |
#+-----------+

data = pd.read_csv('lewis_data.csv.tar.gz').copy()#.set_index('mlno','lewis_data.csv.tar.gz','group','HIVR')
data = data.set_index(['mlno','group','HIVR','lewis_data.csv'])

#+-----------+
#| set  data |
#+-----------+

X = data
y = data.index.get_level_values('HIVR')
features = data.columns.tolist()

#+-----------+
#| split set |
#+-----------+

trainData, testData, trainLabel, testLabel = train_test_split(X, y, random_state=1) 

#+-----------------------+
#| define classifier set |
#+-----------------------+

classify = sklearn.ensemble.RandomForestClassifier(n_estimators=1000
                                                 ,criterion='gini'
                                                 ,max_features='auto'
                                                 ,n_jobs=-1
                                                 ,oob_score=True)

#+-------------+
#| train model |
#+-------------+

model = classify.fit(trainData, trainLabel); print(model)

#+--------------------+
#| show model accuracy|
#+--------------------+

print(
 'Predict on training set: {}\
\nPredict on testing  set: {}'.format(model.score(trainData, trainLabel)
                                      , model.score(testData, testLabel))
    )